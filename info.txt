Eksami alguses peaksite käivitama käsu

  java -jar monitor.jar <teie bitbuket-i kasutajatunnus>

  (olles kataloogis, kuhu te projekti kloonisite).

See programm esitab teie töö automaatselt.

Arvesse läheb seis, mis on esitatud enne kella ... (aega on 1h 45m).

Kui olete töö lõpetanud, siis kontrollige, kas töö on esitatud (nupp "Show Uploaded Data")
ja sulgege monitor.

Küsimused laeb monitor alla (faili questions.txt) siis, kui esimene ekraanitõmmis on üles saadetud.
