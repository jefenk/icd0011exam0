package mvc;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class Ctrl {

    @GetMapping("/")
    public String root() {
        return "root";
    }

    @PostMapping("/transform")
    public String transform(@RequestBody String json) throws JsonProcessingException {
        List<Entry> entries = new ObjectMapper().readValue(json, new TypeReference<>() {});
        List<String> transformedEntries = new ArrayList<>();
        for (Entry entry : entries) {
            if (!(transformedEntries.contains(entry.getDate())) && entry.getCount() > 1) {
                transformedEntries.add(entry.getDate());
            }
        }
        return new ObjectMapper().writeValueAsString(transformedEntries);
    }

}
