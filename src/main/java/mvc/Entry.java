package mvc;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Entry {

    String date;
    int count;

    @Override
    public String toString() {
        return date;
    }

}
